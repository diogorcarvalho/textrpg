import { Context } from './models/artifacts'

// Parses
import { parsesCommandInput } from './parses/parsesCommandInput'

// Services
import { contextService } from './services/contextService'

// Templates
import { regularOutput } from './templates/regularOutputTemplate'

// Commands
import { commandMenu } from './commands/commandMenu'
import { commandHelp } from './commands/commandHelp'
import { commandLook } from './commands/commandLook'
import { commandGo } from './commands/commandGo'
import { commandInventory } from './commands/commandInventory'
import { commandGet } from './commands/commandGet'
import { commandDrop } from './commands/commandDrop'
import { commandUse } from './commands/commandUse'

const INTERVAL_TIME = 3000

let _inputDisplay: (el: HTMLElement) => void

let _handleCls: () => void

let _context: Context

type StateType = {
  intervalId: any
  count: number
}

const state: StateType = {
  intervalId: null,
  count: 0
}

function update(): void {
  
  state.count++

  // _inputDisplay(regularOutput({ text: `[count]: ${state.count}`, color: 'gray', textAlign: 'right' }))
}

async function start(option: { inputDisplay: (el: HTMLElement) => void, handleCls: () => void }) {

  _inputDisplay = option.inputDisplay

  _handleCls = option.handleCls

  await contextService().then((context: Context) => _context = context)

  _inputDisplay(regularOutput({ text: '>>--> TEXTRPG <--<<', color: 'yellow', textAlign: 'center' }))

  setInterval(() => update(), INTERVAL_TIME)
}

function command(input: string) {

  if (!input || !input.trim()) return

  const _arr = parsesCommandInput(input)

  const _command = _arr[0]

  const _args = _arr.filter((v: string, i: number, arr: string[]) => i !== 0)

  switch (_command.toLowerCase()) {
    case 'menu': _inputDisplay(commandMenu(_args, _context))
      break
    case 'help': _inputDisplay(commandHelp(_args, _context))
      break
    case 'look': _inputDisplay(commandLook(_args, _context))
      break
    case 'go': _inputDisplay(commandGo(_args, _context))
      break
    case 'inventory': _inputDisplay(commandInventory(_args, _context))
      break
    case 'get': _inputDisplay(commandGet(_args, _context))
      break
    case 'drop': _inputDisplay(commandDrop(_args, _context))
      break
    case 'use': _inputDisplay(commandUse(_args, _context))
      break
    case 'cls': _handleCls()
      break
    default: _inputDisplay(regularOutput({ text: `"${_command}" não é um comando válido!`, color: 'red' }))
      break
  }
}

function gameEngine() {

  return { state, update, start, command }
}

export default gameEngine
