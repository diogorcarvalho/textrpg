import React from 'react'
import './GameConsole.css'
import gameEngine from './Engine'

const commandHistory: string[] = []

let commandHistoryIndex: number = 0

function inputDisplay(el: HTMLElement): void {
  
  const terminalDisplay = document.getElementById('terminalDisplay') || document.createElement('div')
  
  terminalDisplay.append(el)

  terminalDisplay.scrollTop = terminalDisplay?.scrollHeight - terminalDisplay?.clientHeight
}

function handleKeyDown(ev: React.KeyboardEvent<HTMLInputElement>) {

  console.log('>> commandHistory:')
  
  if (ev.key === 'Enter') {

    if (commandHistory.length < 100) {
      commandHistory.unshift(ev.currentTarget.value)
    } else {
      commandHistory.pop()
    }
    commandHistoryIndex = 0
    
    gameEngine().command(ev.currentTarget.value)
    ev.currentTarget.value = ''
  }

  if (ev.key === 'ArrowUp' && !!commandHistory.length) {

    ev.currentTarget.value = commandHistory[commandHistoryIndex]

    if (commandHistory.length > commandHistoryIndex + 1) {
      commandHistoryIndex++
    } else {
      commandHistoryIndex = 0
    }
  }
}

function handleCls() {

  const terminalDisplay = document.getElementById('terminalDisplay')

  if (terminalDisplay == null) return

  terminalDisplay.innerHTML = ''  
}

export default function GameConsole() {

  gameEngine().start({ inputDisplay, handleCls })
  
  return (
    <div className="terminal">
      <div id="terminalDisplay" className="terminal-display">
        {/* print rows here */}
      </div>
      <div className="terminal-prompt">
        <code>></code>
        <input  autoFocus type="text" onKeyDown={(ev) => handleKeyDown(ev)}/>
      </div>
    </div>
  )
}
