import { Context, Place, Door, Key, NPC, Container, Currencybag, MenuItem } from '../models/artifacts'

export async function contextService(): Promise<Context> {
  
  let context: Context

  return fetch('./data/context.json').then((resp: Response) => resp.json()).then((value: any) => context = new Context(value))
    .then(() => fetch('./data/places.json')).then((resp: Response) => resp.json()).then((arr: any[]) => context.places = arr.map(e => new Place(e)))
    .then(() => fetch('./data/doors.json')).then((resp: Response) => resp.json()).then((arr: any[]) => context.doors = arr.map(e => new Door(e)))
    .then(() => fetch('./data/keys.json')).then((resp: Response) => resp.json()).then((arr: any[]) => context.keys = arr.map(e => new Key(e)))
    .then(() => fetch('./data/npcs.json')).then((resp: Response) => resp.json()).then((arr: any[]) => context.npcs = arr.map(e => new NPC(e)))
    .then(() => fetch('./data/containers.json')).then((resp: Response) => resp.json()).then((arr: any[]) => context.containers = arr.map(e => new Container(e)))
    .then(() => fetch('./data/currencybags.json')).then((resp: Response) => resp.json()).then((arr: any[]) => context.currencybags = arr.map(e => new Currencybag(e)))
    .then(() => fetch('./data/menuItems.json')).then((resp: Response) => resp.json()).then((arr: any[]) => context.menuItems = arr.map(e => new MenuItem(e)))
    .then(() => context)
}