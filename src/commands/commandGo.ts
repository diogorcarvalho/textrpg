import { Context } from '../models/artifacts'
import { regularOutput } from '../templates/regularOutputTemplate'

export function commandGo(args: string[], context: Context): HTMLElement {

  if (!args.length) return regularOutput({ text: `Informa um lugar para ir`, color: 'red' })

  if (args.length !== 1) return regularOutput({ text: `Informa apenas um lugar para ir`, color: 'red' })

  const currentPlace = context.places.find(p => p.id === context.currentPlace.objId)

  const placeRefObj = currentPlace?.ways.find(refObj => refObj.name === args[0])

  if (!placeRefObj) return regularOutput({ text: `Caminho inválido`, color: 'red' })

  context.currentPlace = placeRefObj

  return regularOutput({ text: `Você foi para "${placeRefObj.name}"`, color: 'gray' })
}