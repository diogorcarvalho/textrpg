import { Context, RefObj } from '../models/artifacts'
import { regularOutput } from '../templates/regularOutputTemplate'

export function commandInventory(args: string[], context: Context): HTMLElement {

  // TODO

  const container = document.createElement('div')

  const rowName = regularOutput({ text: `[[[ Inventário ]]]`, color: 'green' })

  container.append(rowName)

  if (!!context.inventory.length) {

    const objNames = context.inventory?.map((obj: RefObj) => obj.name)
  
    const rowObjs = regularOutput({ text: objNames.map(n => ` [${n}]`), color: 'white' })
  
    container.append(rowObjs)
  
  } else {

    container.append(regularOutput({ text: 'sem ítens...', color: 'white' }))
  }

  return container
}