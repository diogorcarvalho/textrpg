import { Context } from '../models/artifacts'
import { regularOutput } from '../templates/regularOutputTemplate'

export function commandDrop(args: string[], context: Context): HTMLElement {

  if (context.inventory.length === 0) {
    return regularOutput({ text: 'Não há objetos em seu inventário.', color: 'red' })  
  }

  if (args.length === 0) {
    return regularOutput({ text: 'Informe o nome do obejto que deseja soltar.', color: 'red' }) 
  }

  const _objRef = context.inventory.find(objRef => objRef.name === args[0])

  if (_objRef == null) {
    return regularOutput({ text: `Não há um objeto "${args[0]}" em seu inventário.`, color: 'red' })
  }

  context.inventory = context.inventory.filter(objRef => objRef !== _objRef)

  const currentPlace = context.places.find(p => p.id === context.currentPlace.objId)

  currentPlace?.objects.push(_objRef)

  return regularOutput({ text: `Você largou "${_objRef.name}" neste local.`, color: 'gray' })
}