import { menuTemplate } from '../templates/menuTemplate'
import { Context } from '../models/artifacts'

export function commandMenu(args: string[], context: Context): HTMLElement {
  
  return menuTemplate()
}