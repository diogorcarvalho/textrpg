import { Context } from '../models/artifacts'
import { regularOutput } from '../templates/regularOutputTemplate'

export function commandLook(args: string[], context: Context): HTMLElement {

  const currentPlace = context.places.find(p => p.id === context.currentPlace.objId)
  
  if (!currentPlace || !currentPlace.objects) return regularOutput({ text: `Erro! Você não se encontra em lugal algum.`, color: 'red' }) 
  
  const div = document.createElement('div')
  
  div.append(regularOutput({ text: `--- [ ${currentPlace.name} ] ---`, color: 'orchid' }))
  
  if (args[0] === undefined) {
    div.append(regularOutput({ text: currentPlace.description, color: 'orange' }))
    return div
  }

  if (args[0] === 'objects') {
    if (currentPlace.objects.length !== 0) {
      div.append(regularOutput({ text: `Objeto(s): ${currentPlace.objects.map(obj => `[${obj.name}]`).join(', ')}`, color: 'white' }))
    } else {
      div.append(regularOutput({ text: `Não há objeto(s) disponível neste lugar...`, color: 'gray' }))
    }
    return div
  }
  
  if (args[0] === 'ways') {
    if (currentPlace.ways.length !== 0) {
      div.append(regularOutput({ text: `Caminho(s): ${currentPlace.ways.map(way => `[${way.name}]`).join(', ')}`, color: 'white' }))
    } else {
      div.append(regularOutput({ text: `Não há caminho(s) disponível neste lugar...`, color: 'gray' }))
    }
    return div
  }

  if (args[0] === 'doors') {
    if (currentPlace.doors.length !== 0) {
      div.append(regularOutput({ text: `Porta(s): ${currentPlace.doors.map(door => `[${door.name}]`).join(', ')}`, color: 'white' }))
    } else {
      div.append(regularOutput({ text: `Não há porta(s) disponível neste lugar...`, color: 'gray' }))
    }
    return div
  }

  if (args[0] === 'npcs') {
    if (currentPlace.npcs.length !== 0) {
      div.append(regularOutput({ text: `Porta(s): ${currentPlace.npcs.map(npc => `[${npc.name}]`).join(', ')}`, color: 'white' }))
    } else {
      div.append(regularOutput({ text: `Não há npc(s) neste lugar...`, color: 'gray' }))
    }
    return div
  }

  return regularOutput({ text: 'Use o comando "look" seguido de um dos argumento: "objects", "ways", "doors", "npcs" ou nenhum argumenta para descrever o local.' })
}