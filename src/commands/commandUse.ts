import { Context } from '../models/artifacts'
import { regularOutput } from '../templates/regularOutputTemplate'

export function commandUse(args: string[], context: Context): HTMLElement {

  // TODO

  return regularOutput({ text: `use ${args[0]} -> ${args[1]}` })
}
