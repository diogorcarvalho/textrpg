import { Context } from '../models/artifacts'
import { regularOutput } from '../templates/regularOutputTemplate'

export function commandGet(args: string[], context: Context): HTMLElement {

  const currentPlace = context.places.find(p => p.id === context.currentPlace.objId)

  if (!currentPlace) return regularOutput({ text: `Erro! Você não se encontra em lugal algum.`, color: 'red' }) 

  if (currentPlace?.objects.length === 0) {
    return regularOutput({ text: 'Não há objetos disponíveis.', color: 'red' })  
  }

  if (args.length !== 1) {
    return regularOutput({ text: 'Informe o nome do obejto que deseja pegar.', color: 'red' }) 
  }

  const objRef = currentPlace?.objects.find(obj => obj.name === args[0])

  if (!objRef) {
    return regularOutput({ text: `Não há um objeto "${args[0]}" neste lugar.`, color: 'red' })
  }

  if (objRef.className === 'Container') {
    return regularOutput({ text: `Não é possível pegar este objeto.`, color: 'red' })
  }
    
  context.inventory.push(objRef)
  
  const objs = currentPlace?.objects.filter(obj => obj.name !== args[0])
  
  currentPlace.objects = objs
  
  return regularOutput({ text: `Você pegou "${objRef.name}" e guardou no seu inventório`, color: 'gray' })
}