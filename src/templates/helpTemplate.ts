import { MenuItem } from '../models/artifacts'

export function helpTemplate(menuItens: MenuItem[]): HTMLElement {
  
  const container = document.createElement('div')
  
  const title = document.createElement('code')
  title.innerHTML = '??? [ HELP ] ???'
  title.style.color = 'green'

  container.append(title)

  menuItens.map(item => {
    const label = document.createElement('code')
    label.innerHTML = `[${item.label}]: `
    label.style.color = 'orange'

    const text = document.createElement('code')
    text.innerHTML = item.text
    text.style.color = 'white'

    const row = document.createElement('div')
    row.append(label)
    row.append(text)

    return row
  }).forEach(el => container.append(el))

  return container
}