
const menuItens = [
  { label: 'abrir', text: 'Abrir uma jogada previamente gravada (ainda não implementado)' },
  { label: 'salvar', text: 'Salvar o jogo corrente (ainda não implementado)' },
  { label: 'sair', text: 'Sair do jogo (ainda não implementado)' }
]

export function menuTemplate(args?: string[]): HTMLElement {

  const container = document.createElement('div')
  
  const title = document.createElement('code')
  title.innerHTML = '::: [ MENU ] :::'
  title.style.color = 'gray'

  container.append(title)

  menuItens.map(item => {
    const label = document.createElement('code')
    label.innerHTML = `[${item.label}]: `
    label.style.color = 'blue'

    const text = document.createElement('code')
    text.innerHTML = item.text
    text.style.color = 'white'

    const row = document.createElement('div')
    row.append(label)
    row.append(text)

    return row
  }).forEach(el => container.append(el))

  return container
}