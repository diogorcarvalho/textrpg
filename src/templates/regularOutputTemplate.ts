
export function regularOutput(options: any): HTMLElement {
  
  const code = document.createElement('code')
  
  code.style.color = options.color

  code.innerHTML = options.text
  
  const row = document.createElement('div')

  if (!!options.textAlign) {
    row.style.textAlign = options.textAlign
  }
  
  row.append(code)

  return row
}