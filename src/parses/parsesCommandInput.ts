/**
 * Não achei uma solução usando REGEX então fiz na unha mesmo.
 * assinado: Diogo o OGRO dos códigos
 */

export function parsesCommandInput(value: string): string[] {
  
  let args = []

  let opened = false

  let arg = ''

  for (let i = 0, len = value.length; i < len; i++) {

    const char = value[i]

    if (opened === true) {
      if (char === '"') {
        if (!!arg.trim()) args.push(arg.split(' ').filter(e => !!e).join(' '))
        opened = false
        arg = ''
      } else {
        arg += char
      }
    } else {
      if (char === '"') {
        opened = true
      } else {
        if (char === ' ') {
          if (!!arg) args.push(arg)
          arg = ''
        } else {
          arg += char
        }
      }
    }
  }

  if (!!arg) {
    args.push(arg)
  }

  return args
}