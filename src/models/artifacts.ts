export class RefObj {

  objId: string
  className: string
  name: string

  constructor(obj?: any) {
    this.objId = !!obj.objId ? obj.objId : null
    this.className = !!obj.className ? obj.className : null
    this.name = !!obj.name ? obj.name : null
  }
}

export class Context {

  currentPlace: RefObj
  inventory: RefObj[]
  inventoryVolumes: number

  places: Place[] = []
  doors: Door[] = []
  keys: Key[] = []
  npcs: NPC[] = []
  containers: Container[] = []
  currencybags: Currencybag[] = []

  menuItems: MenuItem[] = []
  
  constructor(obj?: any) {
    this.currentPlace = new RefObj(obj.currentPlace)
    this.inventory = !!obj.inventory ? obj.inventory.map((object: any) => new RefObj(object)) : null
    this.inventoryVolumes = !!obj.inventoryVolumes ? obj.inventoryVolumes : null
    this.currencybags = !!obj.currencybags ? obj.currencybags.map((currencybag: any) => new RefObj(currencybag)) : null
  }
}

export class Place {

  id: string
  name: string
  description: string
  ways: RefObj[]
  doors: RefObj[]
  objects: RefObj[] // Keys, CurrentBas, etc...
  npcs: RefObj[] 

  constructor(obj?: any) {
    this.id = !!obj.id ? obj.id : null
    this.name = !!obj.name ? obj.name : null
    this.description = !!obj.description ? obj.description : null
    this.ways = !!obj.ways ? obj.ways.map((way: any) => new RefObj(way)) : null
    this.doors = !!obj.doors ? obj.doors.map((door: any) => new RefObj(door)) : null
    this.objects = !!obj.objects ? obj.objects.map((object: any) => new RefObj(object)) : null
    this.npcs = !!obj.npcs ? obj.npcs.map((npc: any) => new RefObj(npc)) : null
  }
}

export class NPC {

  id: string
  name: string
  description: string
  inventory: RefObj[]
  
  constructor(obj?: any) {
    this.id = !!obj.id ? obj.id : null
    this.name = !!obj.name ? obj.name : null
    this.description = !!obj.description ? obj.description : null
    this.inventory = !!obj.inventory ? obj.inventory.map((object: any) => new RefObj(object)) : null
  }
}

export class Key {

  id: string
  name: string
  description: string
  sizeUnit: number
  
  constructor(obj?: any) {
    this.id = !!obj.id ? obj.id : null
    this.name = !!obj.name ? obj.name : null
    this.description = !!obj.description ? obj.description : null
    this.sizeUnit = !!obj.sizeUnit ? obj.sizeUnit : null
  }
}

export class Door {

  id: string
  placeIds: string[]
  name: string
  description: string
  isLocked: boolean
  keyId: string
  
  constructor(obj?: any) {
    this.id = !!obj.id ? obj.id : null
    this.placeIds = !!obj.placeIds ? obj.placeIds : null
    this.name = !!obj.name ? obj.name : null
    this.description = !!obj.description ? obj.description : null
    this.isLocked = !!obj.isLocked ? obj.isLocked : null
    this.keyId = !!obj.keyId ? obj.keyId : null
  }
}

export class Container {

  id: string
  name: string
  description: string
  objects: string[]
  isLocked: boolean
  keyId: string
  volumes: number
  
  constructor(obj?: any) {
    this.id = !!obj.id ? obj.id : null
    this.name = !!obj.name ? obj.name : null
    this.description = !!obj.description ? obj.description : null
    this.objects = !!obj.objects ? obj.objects.map((obj: any) => new RefObj(obj)) : null
    this.isLocked = !!obj.isLocked ? obj.isLocked : null
    this.keyId = !!obj.keyId ? obj.keyId : null
    this.volumes = !!obj.volumes ? obj.volumes : null
  }
}

export class Currencybag {

  id: string
  name: string
  value: string
  coinType: string // 'gold' || 'silver' || 'copper'

  constructor(obj?: any) {
    this.id = !!obj.id ? obj.id : null
    this.name = !!obj.name ? obj.name : null
    this.value = !!obj.value ? obj.value : null
    this.coinType = !!obj.coinType ? obj.coinType : null
  }
}

export class MenuItem {

  label!: string
  text!: string

  constructor(obj?: any) {
    Object.assign(this, obj)
  }
}
